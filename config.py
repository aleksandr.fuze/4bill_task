# -*- coding: utf-8 -*-
import os


basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    AMOUNT_LIMITS_CONFIG = {5: 4000, 10: 5000}
