# -*- coding: utf-8 -*-
import json
import datetime

from app import app


@app.before_first_request
def before_first_request():
    global time_amount, max_time, amount_limits
    time_amount = {}
    amount_limits = app.config['AMOUNT_LIMITS_CONFIG']
    max_time = datetime.timedelta(seconds=max(amount_limits))


@app.route('/request/<int:amount>')
def check_amount(amount):
    now = datetime.datetime.utcnow()
    for dt in list(time_amount.keys()):
        if not dt < now - max_time:
            break
        del time_amount[dt]

    for delta in amount_limits:
        x = sum([time_amount[i] for i in time_amount if i >= now - datetime.timedelta(seconds=delta)]) + amount
        if x > amount_limits[delta]:
            v = amount_limits[delta]
            return json.dumps({"error": f"amount limit is exceeded {v}/{delta}sec"})

    time_amount[now] = amount
    return json.dumps({"result": "OK"})
